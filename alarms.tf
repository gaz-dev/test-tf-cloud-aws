resource "aws_sns_topic" "test" {
  name = "test-sns-topic"
  kms_master_key_id = "alias/aws/sns"
}

resource "aws_cloudwatch_metric_alarm" "asg_no_healthyHost_metric_alarm" {
  alarm_name          = "test-1-healthy-host-0-p1"
  evaluation_periods  = "1"
  period              = "60"
  namespace           = "AWS/AutoScaling"
  metric_name         = "GroupInServiceInstances"
  comparison_operator = "LessThanOrEqualToThreshold"
  threshold           = "0"
  dimensions          = {
    AutoScalingGroupName = aws_autoscaling_group.cwagent_asg.name
  }
  statistic           = "Average"
  tags                = local.default_tags
  alarm_actions       = [aws_sns_topic.test.arn]
}

resource "aws_cloudwatch_metric_alarm" "asg_no_healthyHost_metric_alarm-2" {
  alarm_name          = "test-2-healthy-host-0-p1"
  evaluation_periods  = "1"
  period              = "60"
  namespace           = "AWS/AutoScaling"
  metric_name         = "GroupInServiceInstances"
  comparison_operator = "LessThanOrEqualToThreshold"
  threshold           = "0"
  dimensions          = {
    AutoScalingGroupName = aws_autoscaling_group.cwagent_asg-2.name
  }
  statistic           = "Average"
  tags                = local.default_tags
  alarm_actions       = [aws_sns_topic.test.arn]
}

#resource "aws_cloudwatch_composite_alarm" "example" {
#  alarm_description = "This is a composite alarm!"
#  alarm_name        = "example-composite-alarm"
#
#  alarm_actions = [aws_sns_topic.test.arn]
#
#  alarm_rule  = "ALARM(${aws_cloudwatch_metric_alarm.asg_no_healthyHost_metric_alarm.alarm_name}) AND ALARM(${aws_cloudwatch_metric_alarm.asg_no_healthyHost_metric_alarm-2.alarm_name})"
#
#  depends_on = [aws_cloudwatch_metric_alarm.asg_no_healthyHost_metric_alarm, aws_cloudwatch_metric_alarm.asg_no_healthyHost_metric_alarm-2]
#}

resource "aws_cloudwatch_metric_alarm" "test" {
  alarm_name          = "test-math-ish"
  alarm_description   = "alarm triggerd when all trusted gateway p2 alarms are in an alarm state"
  evaluation_periods  = "1"
  comparison_operator = "LessThanOrEqualToThreshold"
  threshold           = "0"
  alarm_actions       = [aws_sns_topic.test.arn]

  metric_query {
    id          = "e1"
    expression  = "IF((m1 == 0) AND (m2 == 0), 0, 1)"
    label       = "GroupInServiceInstances"
    return_data = "true"
  }

  metric_query {
    id = "m1"
    metric {
      metric_name = "GroupInServiceInstances"
      namespace   = "AWS/AutoScaling"
      period      = "180"
      stat        = "Average"
      dimensions = {
        AutoScalingGroupName = aws_autoscaling_group.cwagent_asg.name
      }
    }
  }

  metric_query {
    id = "m2"
    metric {
      metric_name = "GroupInServiceInstances"
      namespace   = "AWS/AutoScaling"
      period      = "180"
      stat        = "Average"
      dimensions = {
        AutoScalingGroupName = aws_autoscaling_group.cwagent_asg-2.name
      }
    }
  }
}
