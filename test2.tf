##data "template_file" "userdata" {
##  template = file("user_data.sh")
##  vars = {
##    snsTopicArn            = aws_sns_topic.test.arn
##    nameSpace              = "R2D2"
##  }
##}

resource "aws_launch_template" "cwagent_launch_template-2" {
  disable_api_termination = false
  image_id                = "ami-0330ffc12d7224386"
  instance_type           = "t2.micro"
  key_name                = "london_kp"
  name_prefix             = "test-2-"
  tags                    = local.default_tags
##  user_data               = base64encode(data.template_file.userdata.rendered)
  vpc_security_group_ids  = ["sg-09fa836b2a208cf95"]
  block_device_mappings {
    device_name = "/dev/xvda"
    ebs {
      volume_size = 10
      volume_type = "gp2"
    }
  }
  lifecycle {
    create_before_destroy = true
  }
  tag_specifications {
    resource_type = "volume"
    tags          = local.default_tags
  }
}

data "null_data_source" "asg_tags-2" {
  count  = length(keys(local.default_tags))
  inputs = {
    key                 = element(keys(local.default_tags), count.index)
    propagate_at_launch = "true"
    value               = element(values(local.default_tags), count.index)
  }
}

resource "aws_autoscaling_group" "cwagent_asg-2" {
  name                      = "test-2-asg"
  desired_capacity          = 0
  health_check_grace_period = "300"
  max_size                  = 0
  min_size                  = 0
  tags                      = data.null_data_source.asg_tags.*.outputs
  vpc_zone_identifier       = ["subnet-3b6f9f77"]
  enabled_metrics           = ["GroupInServiceInstances"] 
  launch_template {
    id      = aws_launch_template.cwagent_launch_template.id
    version = "$Latest"
  }
  lifecycle {
    create_before_destroy = true
  }
}


##resource "aws_sns_topic" "test" {
##  name = "test-sns-topic"
##}
